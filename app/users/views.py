from rest_framework import generics
from app.users.serializers import UserSerializer

class ListUsers(generics.ListAPIView):

    serializer_class = UserSerializer